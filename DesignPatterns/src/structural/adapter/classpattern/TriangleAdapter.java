package structural.adapter.classpattern;

import structural.adapter.Shape;
import structural.adapter.object.Triangle;

public class TriangleAdapter extends Triangle implements Shape {

    public TriangleAdapter() {
    }

    @Override
    public void draw() {
        this.drawShape();
    }

    @Override
    public String description() {
        return "Triangle object";
    }
}
