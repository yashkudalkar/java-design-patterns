package structural.adapter.classpattern;

import structural.adapter.Circle;
import structural.adapter.Drawing;
import structural.adapter.Rectangle;
import structural.adapter.object.GeometricShapeObjectAdapter;
import structural.adapter.object.Triangle;

public class Test {

    public static void main(String[] args) {
        Drawing drawing=new Drawing();
        drawing.addShape(new Circle());
        drawing.addShape(new Rectangle());
        System.out.println("drawing shapes");
        drawing.draw();
        drawing.addShape(new TriangleAdapter());
        System.out.println("drawing GeometricShape");
        drawing.draw();
    }
}
