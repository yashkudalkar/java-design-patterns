package structural.adapter.object;

import structural.adapter.Circle;
import structural.adapter.Drawing;
import structural.adapter.Rectangle;

public class Test {

    public static void main(String[] args) {
        Drawing drawing=new Drawing();
        drawing.addShape(new Circle());
        drawing.addShape(new Rectangle());
        System.out.println("drawing shapes");
        drawing.draw();
        drawing.addShape(new GeometricShapeObjectAdapter(new Triangle()));
        System.out.println("drawing GeometricShape");
        drawing.draw();
    }
}
