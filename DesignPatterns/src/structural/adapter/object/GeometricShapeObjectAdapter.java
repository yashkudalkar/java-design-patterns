package structural.adapter.object;

import structural.adapter.Shape;

public class GeometricShapeObjectAdapter implements Shape {
    GeometricShape adaptee;

    public GeometricShapeObjectAdapter(GeometricShape geometricShape) {
        this.adaptee = geometricShape;
    }

    @Override
    public void draw() {
        adaptee.drawShape();
    }

    @Override
    public String description() {
        if (adaptee instanceof Triangle) {
            return "Triangle object";
        } else {
            return "Unknown object";
        }
    }
}
