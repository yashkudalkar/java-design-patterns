package structural.adapter.object;

public interface GeometricShape {

    double area();
    void drawShape();
}
