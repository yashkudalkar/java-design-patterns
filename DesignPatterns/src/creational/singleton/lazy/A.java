package creational.singleton.lazy;

import java.io.Serializable;

public class A implements Serializable {

    private static A obj;
    private String name;

    private A() {
        this.name ="A singleton";
    }

    public static A getInstanceA(){
        if(obj == null){
            synchronized (A.class){
                if(obj == null){
                    obj =new A();
                }
            }
        }
        return obj;
    }

    protected Object readResolve() {
        return getInstanceA();
    }

    public static void main(String[] args) {
        System.out.println(A.getInstanceA().name);
    }

}
