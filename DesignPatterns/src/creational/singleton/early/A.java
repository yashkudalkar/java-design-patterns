package creational.singleton.early;

import java.io.Serializable;

public class A implements Serializable {

    private static A obj = new A();
    private String name;

    private A() {
        this.name ="A singleton";
    }

    public static A getIntanceA(){
        return obj;
    }

    protected Object readResolve() {
        return getIntanceA();
    }


    public static void main(String[] args) {
        System.out.println(""+A.getIntanceA());
    }
}
